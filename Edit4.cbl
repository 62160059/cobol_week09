       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Listing9-4.
       AUTHOR. NATCHAMON.


       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  STARS       PIC *****.
       01  NumOfStars  PIC 9.
       
       PROCEDURE DIVISION .
       BEGIN.
           PERFORM VARYING NumOfStars FROM 0 BY 1 UNTIL NumOfStars > 5
              COMPUTE STARS = 10 ** (4 - NumOfStars)
              INSPECT STARS CONVERTING  "10" TO SPACES 
              DISPLAY NumOfStars " = " STARS

           END-PERFORM
           STOP RUN
           .